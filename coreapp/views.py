# views.py
from coreapp import app
from flask import Flask,request
from datetime import *

@app.route('/dow')
def dow():
	
	try:
		day = request.args['day']
		month = request.args['month']
		year = request.args['year']
	
	except Exception:
		return "Form empty"
	try:
		weekDay = " "
		d = datetime(int(year), int(month), int(day))
		d = d.weekday()
	
		if d == 0:
			weekDay = "Monday"
		elif d == 1:
			weekDay = "Tuesday"
		elif d == 2:
			weekDay = "Wednesday"
		elif d == 3:
			weekDay = "Thursday"
		elif d == 4:
			weekDay = "Friday"
		elif d == 5:
			weekDay = "Saturday"
		elif d == 6:
			weekDay =  "Sunday"
	except Exception:
		return "Something went wrong!"

	webpage = """
	<!DOCTYPE html>
		<html lang="en-US">
		<head>
		<title>Day of the week.</title>
		<meta charset="utf-8">
		</head>
		<body>
			<h1>Day of the Week</h1>
			<p>""" + weekDay + """ </p> 
		</body> 
		</html> """
		
				
		
	return webpage
	
@app.route('/')
def index():
    page = """
    <DOCTYPE! html>
    <html lang="en-US">
    <head>
    <title>Hello World Page</title>
    <meta charset=utf-8">
    </head>
    <body>
  <h1> Enter a date </h1>
	<h3> Enter a date below and submit it and we will find the day of the week </h3>
	
	<form action="/dow" method="get">
		
		<select name = "day">
			
			<option value = "1"> 1 </option>
			<option value = "2"> 2 </option>
			<option value = "3"> 3 </option>
			<option value = "4"> 4 </option>
			<option value = "5"> 5 </option>
			<option value = "6"> 6 </option>
			<option value = "7"> 7 </option>
			<option value = "8"> 8 </option>
			<option value = "9"> 9 </option>
			<option value = "10"> 10 </option>
			<option value = "11"> 11 </option>
			<option value = "12"> 12 </option>
			<option value = "13"> 13 </option>
			<option value = "14"> 14 </option>
			<option value = "15"> 15 </option>
			<option value = "16"> 16 </option>
			<option value = "17"> 17 </option>
			<option value = "18"> 18 </option>
			<option value = "19"> 19 </option>
			<option value = "20"> 20 </option>
			<option value = "21"> 21 </option>
			<option value = "22"> 22 </option>
			<option value = "23"> 23 </option>
			<option value = "24"> 24 </option>
			<option value = "25"> 25 </option>
			<option value = "26"> 26 </option>
			<option value = "27"> 27 </option>
			<option value = "28"> 28 </option>
			<option value = "29"> 29 </option>
			<option value = "30"> 30 </option>
			<option value = "31"> 31 </option>
		</select>
		
		<select name = "month">
			
			<option value = "1"> 1 </option>
			<option value = "2"> 2 </option>
			<option value = "3"> 3 </option>
			<option value = "4"> 4 </option>
			<option value = "5"> 5 </option>
			<option value = "6"> 6 </option>
			<option value = "7"> 7 </option>
			<option value = "8"> 8 </option>
			<option value = "9"> 9 </option>
			<option value = "10"> 10 </option>
			<option value = "11"> 11 </option>
			<option value = "12"> 12 </option>	
		
		</select>
	
		<select name = "year">
			
			<option value = "2010"> 2010 </option>
			<option value = "2011"> 2011 </option>
			<option value = "2012"> 2012 </option>
			<option value = "2013"> 2013 </option>
			<option value = "2014"> 2014 </option>
			<option value = "2015"> 2015 </option>
			
		</select>
		
		<input type = "submit"/>
		
	</form> 
    </body>
    </html>
    """
    return page
